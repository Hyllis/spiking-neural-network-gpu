# README #

A simple Spiking Neural Network using GPU parallelism (CUDA).

There's an optional graphical interface (SFML).

[Here's][1] a video with a small, hand-crafted network - although it can support  40k neurons + 600k synapses and more.

## Compilation ##

**With visualizer**

* `make sfml`
* `make`
* `make run` (or ```make env``` within backticks, then `./cudasnn`)

**Without visualizer**

* `make tui`
* `./cudasnn`

## Known issues ##

* The Linux SFML version may or may not work (apparently depending of the glibc version).

[1]: https://www.youtube.com/watch?v=MqnbHmJ8ZaI