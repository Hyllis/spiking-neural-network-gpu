#include <stdlib.h>
#include <time.h>
#include "SNN.hpp"

int			randab(int a, int b)
{
	return (rand() % (b - a) + a);
}

double		frandab(double a, double b)
{
	return ((rand() / (double)RAND_MAX) * (b - a) + a);
}

void		init_neuron(t_neuron_info *nfo, t_neuron *ret, int id, int x, int y,
					char type)
{
	nfo->x = x;
	nfo->y = y;
	nfo->z = 0;
	nfo->gid = id / 100;
	ret->in_value = frandab(.8f, 3.f);
	ret->in_time = randab(50, 100);
	ret->next_time = ret->in_time * frandab(0.2f, 1.0f);
	ret->action_potential = 0.0f;
	ret->threshold = frandab(6.5f, 10.5f);
	ret->weight = frandab(0.3f, 1.2f);
	ret->type = type;
	nfo->group = (nfo->gid == 0 ? INPUT : (nfo->gid == 1 ? OUTPUT : HIDDEN));
	ret->carry = 0;
}

void			init_synapse(t_synapse *ret, int in, int out)
{
	ret->id_in = in;
	ret->id_out = out;
	ret->axonal_delay = randab(5, 30);
	ret->status = false;
}

t_network		*generate_network(void)
{
	t_network	*ret;

	if (!(ret = (t_network*)malloc(sizeof(t_network))))
		exit(1);
	ret->timestep = 0;
	ret->group_size = 100;

	/* Alloc */
	ret->neur_count = 20000;
	if (!(ret->neurons = (t_neuron*)malloc(sizeof(t_neuron)
			* ((ret->neur_count + 511) & ~511)))
		|| !(ret->neurons_info = (t_neuron_info*)malloc(sizeof(t_neuron_info)
			* ((ret->neur_count + 511) & ~511))))
		return (NULL);
	ret->syn_count = 400000;
	if (!(ret->synapses = (t_synapse*)malloc(sizeof(t_synapse)
			* ((ret->syn_count + 511) & ~511))))
		return (NULL);
	if (!(ret->spikes = (t_spike*)malloc(sizeof(t_spike)
			* ((ret->syn_count + 511) & ~511) * SPIKE_BUFFER)))
		return (NULL);

	/* Assign */
	for (int i = 0; i < ret->neur_count; ++i)
		init_neuron(&(ret->neurons_info[i]), &(ret->neurons[i]), i,
				(i / 12) * 11, (i % 60) * 18,
				randab(0, 100) >= STIMULUS_RATIO ? INHIBITION : STIMULUS);

	for (int i = 0; i < ret->syn_count; ++i)
	{
		int in = randab(0, ret->neur_count);
		int out = (randab(0, 2) || ret->neurons_info[in].gid == INPUT
				? randab(0, ret->neur_count)
				: randab((int)(in / ret->group_size) * ret->group_size,
					((int)(in / ret->group_size) + 1) * ret->group_size));
		while (in == out)
			out = randab(0, ret->neur_count);
		init_synapse(&(ret->synapses[i]), in, out);
	}

	for (int i = 0; i < ret->syn_count * 4; ++i)
		ret->spikes[i].active = false;

	return (ret);
}
