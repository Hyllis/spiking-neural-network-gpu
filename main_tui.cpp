#include "SNN.hpp"

int			main(void)
{
	t_network	*nw;

	srand(time(NULL));
	if (!(nw = generate_network()))
		return (1);
	while (43)
	{
		simulate_network(nw);
		++nw->timestep;
	}
	return (0);
}
